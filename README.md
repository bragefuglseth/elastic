# Elastic

![icon](data/icons/app.drey.Elastic.svg)
<p>Design spring animations.</p>

<a href="https://flathub.org/apps/details/app.drey.Elastic"><img height="51" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.svg"/> </a>

## Screenshots

<div align="center">
![screenshot](data/screenshots/transforms.png)
</div>

## Hack on Elastic

To build the development version of Elastic and hack on the code see the
[general guide](https://wiki.gnome.org/Newcomers/BuildProject) for building
GNOME apps with Flatpak and GNOME Builder.

You are expected to follow the GNOME Code of Conduct when participating in
project spaces.
