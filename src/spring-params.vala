// This file is part of Elastic. License: GPL-3.0+.

public class Elastic.SpringParams : Object {
    private const double DEFAULT_DAMPING = 10;
    private const double DEFAULT_MASS = 1;
    private const double DEFAULT_STIFFNESS = 100;
    private const double DEFAULT_EPSILON = 0.001;
    private const double DEFAULT_VELOCITY = 0;

    private double _damping = DEFAULT_DAMPING;
    public double damping {
        get { return _damping; }
        set {
            if (damping == value)
                return;

            bool was_modified = is_modified;

            _damping = value;

            notify_property ("damping-ratio");

            if (was_modified != is_modified)
                notify_property ("is-modified");

            changed ();
        }
    }

    public double damping_ratio {
        get { return damping_ratio_for_damping (damping); }
        set {
            if (damping_ratio == value)
                return;

            damping = damping_for_damping_ratio (value);
        }
    }

    private double _mass = DEFAULT_MASS;
    public double mass {
        get { return _mass; }
        set {
            if (mass == value)
                return;

            double old_damping_ratio = damping_ratio;
            bool was_modified = is_modified;

            _mass = value;

            if (settings.get_boolean ("use-damping"))
                notify_property ("damping-ratio");
            else
                damping = damping_for_damping_ratio (old_damping_ratio);

            if (was_modified != is_modified)
                notify_property ("is-modified");

            changed ();
        }
    }

    private double _stiffness = DEFAULT_STIFFNESS;
    public double stiffness {
        get { return _stiffness; }
        set {
            if (stiffness == value)
                return;

            double old_damping_ratio = damping_ratio;
            bool was_modified = is_modified;

            _stiffness = value;

            if (settings.get_boolean ("use-damping"))
                notify_property ("damping-ratio");
            else
                damping = damping_for_damping_ratio (old_damping_ratio);

            if (was_modified != is_modified)
                notify_property ("is-modified");

            changed ();
        }
    }

    private double _epsilon = DEFAULT_EPSILON;
    public double epsilon {
        get { return _epsilon; }
        set {
            if (epsilon == value)
                return;

            bool was_modified = is_modified;

            _epsilon = value;

            if (was_modified != is_modified)
                notify_property ("is-modified");

            changed ();
        }
    }

    private double _velocity = DEFAULT_VELOCITY;
    public double velocity {
        get { return _velocity; }
        set {
            if (velocity == value)
                return;

            bool was_modified = is_modified;

            _velocity = value;

            if (was_modified != is_modified)
                notify_property ("is-modified");

            changed ();
        }
    }

    public bool is_modified {
        get {
            return damping != DEFAULT_DAMPING ||
                   mass != DEFAULT_MASS ||
                   stiffness != DEFAULT_STIFFNESS ||
                   epsilon != DEFAULT_EPSILON ||
                   velocity != DEFAULT_VELOCITY;
        }
    }

    public signal void changed ();

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Elastic");
    }

    public void reset_to_defaults () {
        mass = DEFAULT_MASS;
        stiffness = DEFAULT_STIFFNESS;
        damping = DEFAULT_DAMPING;
        epsilon = DEFAULT_EPSILON;
        velocity = DEFAULT_VELOCITY;
    }

    public double damping_ratio_for_damping (double damping) {
        double critical_damping = 2 * Math.sqrt (mass * stiffness);

        return damping / critical_damping;
    }

    public double damping_for_damping_ratio (double damping_ratio) {
        double critical_damping = 2 * Math.sqrt (mass * stiffness);

        return damping_ratio * critical_damping;
    }

    public void apply (Adw.SpringAnimation animation) {
        animation.spring_params = new Adw.SpringParams.full (
            damping, mass, stiffness
        );
        animation.initial_velocity = velocity;
        animation.epsilon = epsilon;
    }

    public bool is_epsilon_modified () {
        return epsilon != DEFAULT_EPSILON;
    }

    public bool is_velocity_modified () {
        return velocity != DEFAULT_VELOCITY;
    }
}
