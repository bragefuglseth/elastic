// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/basic-view.ui")]
public class Elastic.BasicView : Adw.Bin {
    public SpringParams spring { get; set; }
    public bool showing_graph { get; set; }

    private bool _compact;
    public bool compact {
        get { return _compact; }
        set {
            if (compact == value)
                return;

            _compact = value;

            multi_layout_view.layout_name = compact ? "narrow" : "wide";
        }
    }

    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned TransformsView transforms_view;
    [GtkChild]
    private unowned GraphView graph_view;
    [GtkChild]
    private unowned Adw.MultiLayoutView multi_layout_view;
    [GtkChild]
    private unowned PropertyRow velocity_row;

    private BindingGroup spring_bindings;

    static construct {
        install_property_action ("editor.toggle-graph", "showing-graph");
        install_action ("editor.run-animation", null, widget => {
            var self = widget as BasicView;

            if (self.showing_graph)
                self.graph_view.run_animation ();
            else
                self.transforms_view.run_animation ();
        });

        set_css_name ("basic-view");
    }

    construct {
        spring_bindings = new BindingGroup ();

        spring_bindings.bind (
            "velocity", velocity_row,
            "value", SYNC_CREATE | BIDIRECTIONAL
        );

        bind_property ("spring", spring_bindings, "source", DEFAULT);

        notify["showing-graph"].connect (() => {
            if (showing_graph)
                stack.visible_child = graph_view;
            else
                stack.visible_child = transforms_view;
        });
    }
}
