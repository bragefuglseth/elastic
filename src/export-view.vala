// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/export-view.ui")]
public class Elastic.ExportView : Adw.Bin {
    private SpringParams _spring;
    public SpringParams spring {
        get { return _spring; }
        set {
            if (spring == value)
                return;

            if (spring != null)
                spring.changed.disconnect (update_code);

            _spring = value;

            if (spring != null)
                spring.changed.connect (update_code);

            update_code ();
        }
    }

    public bool use_damping { get; set; }

    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned GtkSource.Buffer c_buffer;
    [GtkChild]
    private unowned GtkSource.Buffer js_buffer;
    [GtkChild]
    private unowned GtkSource.Buffer python_buffer;
    [GtkChild]
    private unowned GtkSource.Buffer rust_buffer;
    [GtkChild]
    private unowned GtkSource.Buffer vala_buffer;

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Elastic");

        settings.bind ("use-damping", this, "use-damping", GET);
        settings.bind ("last-language", stack, "visible-child-name", DEFAULT);

        var lang_manager = GtkSource.LanguageManager.get_default ();

        c_buffer.language = lang_manager.get_language ("c");
        js_buffer.language = lang_manager.get_language ("js");
        python_buffer.language = lang_manager.get_language ("python");
        rust_buffer.language = lang_manager.get_language ("rust");
        vala_buffer.language = lang_manager.get_language ("vala");

        var style_manager = Adw.StyleManager.get_default ();
        style_manager.notify["dark"].connect (update_style_scheme);

        update_style_scheme ();

        notify["use-damping"].connect (update_code);
    }

    static construct {
        typeof (InlineStackSwitcher).ensure ();

        install_action ("editor.copy-snippet", null, widget => {
            ((ExportView) widget).copy_snippet ();
        });

        set_css_name ("export-view");
    }

    private void update_style_scheme () {
        var style_manager = Adw.StyleManager.get_default ();
        var name = style_manager.dark ? "Adwaita-dark" : "Adwaita";
        var scheme_manager = GtkSource.StyleSchemeManager.get_default ();

        c_buffer.style_scheme = scheme_manager.get_scheme (name);
        js_buffer.style_scheme = scheme_manager.get_scheme (name);
        python_buffer.style_scheme = scheme_manager.get_scheme (name);
        rust_buffer.style_scheme = scheme_manager.get_scheme (name);
        vala_buffer.style_scheme = scheme_manager.get_scheme (name);
    }

    private inline string format_double (double value, int digits) {
        var format = @"%.$(digits)lf";

        Intl.setlocale (NUMERIC, "C");
        var ret = format.printf (value);
        Intl.setlocale (NUMERIC, "");

        return ret;
    }

    private string create_code (string language) {
        try {
            var template_path = @"/app/drey/Elastic/templates/$language.tmpl";

            var template = new Template.Template (null);
            template.parse_resource (template_path, null);

            var scope = new Template.Scope ();
            scope.set_string ("damping", format_double (spring.damping, 1));
            scope.set_string ("damping_ratio", format_double (spring.damping_ratio, 2));
            scope.set_string ("mass", format_double (spring.mass, 1));
            scope.set_string ("stiffness", format_double (spring.stiffness, 1));
            scope.set_string ("epsilon", format_double (spring.epsilon, 5));
            scope.set_string ("velocity", format_double (spring.velocity, 1));

            scope.set_boolean ("use_damping", use_damping);
            scope.set_boolean ("has_epsilon", spring.is_epsilon_modified ());
            scope.set_boolean ("has_velocity", spring.is_velocity_modified ());

            return template.expand_string (scope).chomp ();
        } catch (Error e) {
            error ("Error parsing %s template: %s", language, e.message);
        }
    }

    private void update_code () {
        c_buffer.text = create_code ("c");
        js_buffer.text = create_code ("javascript");
        python_buffer.text = create_code ("python");
        rust_buffer.text = create_code ("rust");
        vala_buffer.text = create_code ("vala");
    }

    private void copy_snippet () {
        GtkSource.Buffer buffer;

        if (stack.visible_child_name == "cc")
            buffer = c_buffer;
        else if (stack.visible_child_name == "javascript")
            buffer = js_buffer;
        else if (stack.visible_child_name == "python")
            buffer = python_buffer;
        else if (stack.visible_child_name == "rust")
            buffer = rust_buffer;
        else if (stack.visible_child_name == "vala")
            buffer = vala_buffer;
        else
            assert_not_reached ();

        var clipboard = get_clipboard ();

        clipboard.set_text (buffer.text);

        ((Window) get_root ()).show_copy_toast ();
    }
}
