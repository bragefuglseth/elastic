// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/preferences-dialog.ui")]
public class Elastic.PreferencesDialog : Adw.PreferencesDialog {
    public bool use_damping { get; set; }

    private Settings settings;

    construct {
        settings = new Settings ("app.drey.Elastic");

        settings.bind ("use-damping", this, "use-damping", DEFAULT);
    }
}
