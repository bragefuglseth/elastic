// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/graph-view.ui")]
public class Elastic.GraphView : Adw.Bin {
    private const int TOP_PADDING = 24;
    private const double START_VALUE = 0;
    private const double END_VALUE = 1;
    private const int N_POINTS = 1000;

    private const string YELLOW_1 = "#f9f06b";
    private const string YELLOW_5 = "#e5a50a";

    private SpringParams _spring;
    public SpringParams spring {
        get { return _spring; }
        set {
            if (spring == value)
                return;

            if (spring != null)
                spring.changed.disconnect (prepare_graph);

            _spring = value;

            if (spring != null)
                spring.changed.connect (prepare_graph);

            prepare_graph ();
        }
    }

    [GtkChild]
    private unowned Gtk.Widget label_box;
    [GtkChild]
    private unowned Gtk.Label duration_label;
    [GtkChild]
    private unowned Gtk.Label min_label;
    [GtkChild]
    private unowned Gtk.Label max_label;

    private struct GraphPoint {
        int64 time;
        double value;
    }

    private Adw.SpringAnimation animation;
    private GraphPoint[]? points;
    private int64 start_time;
    private int64 current_time;
    private int64 duration;
    private double min;
    private double max;

    private int last_index;

    construct {
        animation = new Adw.SpringAnimation (
            this, START_VALUE, END_VALUE, new Adw.SpringParams.full (1, 1, 1),
            new Adw.CallbackAnimationTarget (value => {
                current_time = get_frame_clock ().get_frame_time () - start_time;
                queue_draw ();
            })
        );

        animation.done.connect (() => {
            current_time = int64.MAX;
            queue_draw ();
        });

        animation.follow_enable_animations_setting = false;

        notify["scale-factor"].connect (queue_draw);

        var style_manager = Adw.StyleManager.get_default ();
        style_manager.notify["dark"].connect (queue_draw);
        style_manager.notify["high-contrast"].connect (queue_draw);
    }

    static construct {
        set_css_name ("graph-view");
    }

    public void run_animation () {
        start_time = get_frame_clock ().get_frame_time ();

        animation.play ();
    }

    private void set_min (double min) {
        this.min = min;
        min_label.label = _("Min: %.2lf").printf (min);
    }

    private void set_max (double max) {
        this.max = max;
        max_label.label = _("Max: %.2lf").printf (max);
    }

    private void prepare_graph () {
        animation.reset ();

        points = {};

        min = double.min (START_VALUE, END_VALUE);
        max = double.max (START_VALUE, END_VALUE);
        start_time = current_time = 0;
        last_index = 0;

        spring.apply (animation);

        uint estimated = animation.estimated_duration;

        if (estimated == uint.MAX) {
            duration = -1;

            label_box.opacity = 0;
        } else {
            duration = (int64) (estimated * 1000);
            duration_label.label = _("Duration: %.0lf ms").printf (
                duration / 1000.0
            );

            // Extrapolate the curve for a second before and after the animation
            // to ensure the curve is smooth at the ends.
            double initial_velocity = animation.calculate_velocity (0);
            double final_velocity = animation.calculate_velocity (estimated);

            GraphPoint initial_point = {
                -1000000,
                START_VALUE - initial_velocity
            };

            points += initial_point;

            for (int i = 0; i <= N_POINTS; i++) {
                uint t = (uint) Math.round ((double) i * estimated / N_POINTS);
                double value = animation.calculate_value (t);
                int64 time = (int64) t * 1000;

                GraphPoint point = { time, value };
                points += point;

                if (value < min)
                    min = value;

                if (value > max)
                    max = value;
            }

            GraphPoint final_point = {
                (estimated + 1000) * 1000,
                END_VALUE + final_velocity
            };

            points += final_point;

            label_box.opacity = 1;
        }

        set_min (min);
        set_max (max);

        queue_draw ();
    }

    private inline double inverse_lerp (double a, double b, double t) {
        return (t - a) / (b - a);
    }

    private inline float transform_y (float height, double y) {
        float bottom_padding = label_box.get_height ();

        height -= bottom_padding;

        if (min == max)
            return height / 2;

        double start = height - (height - TOP_PADDING) * inverse_lerp (
            min, max, START_VALUE
        );
        double end = height - (height - TOP_PADDING) * inverse_lerp (
            min, max, END_VALUE
        );

        // Round to physical pixels so we get better precision on hidpi
        start = Math.round (start * scale_factor) / scale_factor;
        end = Math.round (end * scale_factor) / scale_factor;

        return (float) Adw.lerp (start, end, y);
    }

    private inline float transform_x (float width, double x) {
        return (float) x * width / duration;
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        int width = get_width ();
        int height = get_height ();

        bool dark = Adw.StyleManager.get_default ().dark;
        bool hc = Adw.StyleManager.get_default ().high_contrast;
        float y1 = transform_y (height, START_VALUE);
        float y2 = transform_y (height, END_VALUE);

        assert (points != null);

        if (points.length == 0)
            return;

        snapshot.translate ({ 0, 0.5f });

        var color = get_color ();

        // Dashed top and bottom lines

        var builder = new Gsk.PathBuilder ();
        builder.move_to (0, y1);
        builder.rel_line_to (width, 0);
        builder.move_to (0, y2);
        builder.rel_line_to (width, 0);
        var path = builder.to_path ();

        var stroke = new Gsk.Stroke (1);
        stroke.set_dash ({ 4, 2 });

        float dashed_alpha = hc ? 0.5f : 0.15f;
        snapshot.append_stroke (
            path, stroke,
            { color.red, color.green, color.blue, color.alpha * dashed_alpha }
        );

        // Placeholder line

        builder = new Gsk.PathBuilder ();

        for (int i = last_index; i < points.length; i++) {
            float x = transform_x (width, points[i].time);
            float y = transform_y (height, points[i].value);

            if (i == last_index)
                builder.move_to (x, y);

            builder.line_to (x, y);
        }

        path = builder.to_path ();

        float path_alpha = hc ? 0.65f : 0.25f;
        snapshot.append_stroke (
            path, new Gsk.Stroke (1),
            { color.red, color.green, color.blue, color.alpha * path_alpha }
        );

        if (current_time == 0)
            return;

        // Yellow part

        float last_x = 0;
        float last_y = 0;
        builder = new Gsk.PathBuilder ();

        for (int i = 0; i < points.length; i++) {
            if (points[i].time > current_time) {
                last_index = i - 1;
                break;
            }

            float x = transform_x (width, points[i].time);
            float y = transform_y (height, points[i].value);

            last_x = x;
            last_y = y;

            if (i == 0)
                builder.move_to (x, y);

            builder.line_to (x, y);
        }

        var filled_path = builder.to_path ();

        builder = new Gsk.PathBuilder ();
        builder.add_path (filled_path);
        builder.move_to (last_x, last_y);
        builder.line_to (last_x, height);
        builder.line_to (transform_x (width, points[0].time), height);
        builder.line_to (
            transform_x (width, points[0].time),
            transform_y (height, points[0].value)
        );

        Gdk.RGBA fill_color = {};
        fill_color.parse (dark ? YELLOW_5 : YELLOW_1);
        fill_color.alpha = dark ? 0.25f : 0.5f;

        path = builder.to_path ();
        snapshot.append_fill (path, EVEN_ODD, fill_color);

        Gdk.RGBA stroke_color = {};
        stroke_color.parse (dark ? YELLOW_1 : YELLOW_5);

        stroke = new Gsk.Stroke (2);
        snapshot.append_stroke (filled_path, stroke, stroke_color);
    }

    protected override void unmap () {
        base.unmap ();

        prepare_graph ();
    }
}
