// This file is part of Elastic. License: GPL-3.0+.

[GtkTemplate (ui = "/app/drey/Elastic/window.ui")]
public class Elastic.Window : Adw.ApplicationWindow {
    public SpringParams spring { get; set; }

    [GtkChild]
    private unowned Gtk.Stack stack;
    [GtkChild]
    private unowned BasicView basic_view;
    [GtkChild]
    private unowned InteractiveView interactive_view;
    [GtkChild]
    private unowned Adw.ToastOverlay toast_overlay;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    static construct {
        typeof (InlineStackSwitcher).ensure ();
        typeof (PropertiesView).ensure ();
        typeof (ExportView).ensure ();

        install_action ("editor.reset", null, widget => {
            ((Window) widget).spring.reset_to_defaults ();
        });
        install_action ("editor.basic-view", null, widget => {
            var self = widget as Window;

            self.stack.visible_child = self.basic_view;
        });
        install_action ("editor.interactive-view", null, widget => {
            var self = widget as Window;

            self.stack.visible_child = self.interactive_view;
        });

        add_binding_action (Gdk.Key.W, CONTROL_MASK, "window.close", null);
    }

    construct {
        spring = new SpringParams ();

        spring.notify["is-modified"].connect (() => {
            action_set_enabled ("editor.reset", spring.is_modified);
        });

        action_set_enabled ("editor.reset", false);

        stack.notify["visible-child"].connect (() => {
            action_set_enabled (
                "editor.basic-view",
                stack.visible_child != basic_view
            );
            action_set_enabled (
                "editor.interactive-view",
                stack.visible_child != interactive_view
            );
        });
    }

    protected override void snapshot (Gtk.Snapshot snapshot) {
        base.snapshot (snapshot);

        interactive_view.snapshot_toplevel (this, snapshot);
    }

    public void show_copy_toast () {

        var toast = new Adw.Toast (_("Copied to clipboard"));

        toast_overlay.add_toast (toast);
    }
}
