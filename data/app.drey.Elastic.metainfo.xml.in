<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop">
  <id>app.drey.Elastic</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <launchable type="desktop-id">app.drey.Elastic.desktop</launchable>
  <developer id="app.drey.alicem">
    <name>Alice Mikhaylenko</name>
  </developer>

  <name>Elastic</name>
  <summary>Design spring animations</summary>

  <description>
    <p>
      Elastic allows to design and export spring physics-based animations to use
      with libadwaita.
    </p>
    <p>Features:</p>
    <ul>
      <li>Preview translation, rotation and scaling transformations.</li>
      <li>See the animation curve and duration on a graph.</li>
      <li>Drag a handle to see it return back with the spring physics.</li>
      <li>Export C, JavaScript, Python, Vala or Rust code.</li>
    </ul>
  </description>

  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/World/elastic/raw/main/data/screenshots/transforms.png</image>
      <caption>Initial screen</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/elastic/raw/main/data/screenshots/graph.png</image>
      <caption>Animation graph</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/elastic/raw/main/data/screenshots/interactive.png</image>
      <caption>Interactive preview</caption>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/World/elastic/raw/main/data/screenshots/export.png</image>
      <caption>Code export</caption>
    </screenshot>
  </screenshots>

  <releases>
    <release version="0.1.5" date="2024-03-20">
      <description>
        <p>Visual refinements to match newer GNOME apps</p>
        <p>Work around the bug with clipped information labels</p>
        <p>New translations:</p>
        <ul>
          <li>British English</li>
          <li>Friulian</li>
          <li>Georgian</li>
          <li>Greek</li>
          <li>Hebrew</li>
          <li>Romanian</li>
          <li>Swedish</li>
        </ul>
      </description>
    </release>
    <release version="0.1.4" date="2023-09-21">
      <description>
        <p>Visual refinements to match newer GNOME apps</p>
        <p>Fix Rust template</p>
        <p>Fix typography</p>
        <p>New translations:</p>
        <ul>
          <li>Basque</li>
          <li>Brazilian Portuguese</li>
          <li>Chinese (China)</li>
          <li>Danish</li>
          <li>Dutch</li>
          <li>French</li>
          <li>Georgian</li>
          <li>German</li>
          <li>Hungarian</li>
          <li>Indonesian</li>
          <li>Italian</li>
          <li>Portuguese</li>
          <li>Russian</li>
          <li>Slovenian</li>
          <li>Spanish</li>
          <li>Turkish</li>
          <li>Ukrainian</li>
        </ul>
      </description>
    </release>
    <release version="0.1.3" date="2023-03-15">
      <description>
        <p>Spelling fixes:</p>
        <ul>
          <li>"progresss" the C example</li>
          <li>"1381ms" with no space in the graph view</li>
        </ul>
      </description>
    </release>
    <release version="0.1.2" date="2023-02-25">
      <description>
        <p>Fixed 0.1.1 release description</p>
      </description>
    </release>
    <release version="0.1.1" date="2023-02-25">
      <description>
        <p>Changes:</p>
        <ul>
          <li>Add Ctrl+W shortcut for closing the window</li>
          <li>Clarify tooltips for information buttons</li>
          <li>Add a tooltip for the reset button</li>
          <li>Have a scale mark for default damping/damping ratio</li>
          <li>Fix symbolic icon</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" date="2023-02-21">
      <description>
        <p>Initial release</p>
      </description>
    </release>
  </releases>

  <url type="homepage">https://gitlab.gnome.org/World/elastic</url>
  <url type="bugtracker">https://gitlab.gnome.org/World/elastic/-/issues</url>
  <url type="donation">https://www.gnome.org/donate/</url>
  <url type="vcs-browser">https://gitlab.gnome.org/World/elastic</url>
  <url type="translate">https://l10n.gnome.org/module/elastic/</url>

  <content_rating type="oars-1.1" />
  <translation type="gettext">elastic</translation>

  <recommends>
    <control>pointing</control>
    <control>keyboard</control>
    <control>touch</control>
  </recommends>

  <requires>
    <display_length compare="ge">360</display_length>
  </requires>

  <branding>
    <color type="primary" scheme_preference="light">#99c1f1</color>
    <color type="primary" scheme_preference="dark">#0350a4</color>
  </branding>
</component>
